package Steps;

import Pages.*;
import Storage.Data;
import Storage.DriverManager;
import Utils.DriverActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.List;

public class HightestSteps {

    @When("I open {string} website")
    public void iOpenWebsite(String url) {
        DriverManager.driver.get(url);
        if(url.contains("hightest")){
            HightestHomePage.getAcceptCookiesButton().click();
        }else if(url.contains("yopmail")){
            YopmailPage.getAcceptCookiesButton().click();
        }
    }

    @And("I acces french ISTQB fondation test")
    public void iAccesISTQBFondationTest() {
        HightestHomePage.getMenuByTitle("Toolbox").click();
        ToolboxPage.getISTQBFondationByLanguage("Français").click();
    }

    @Then("the test is correctly displayed")
    public void theTestIsCorrectlyDisplayed() {
        DriverActions.switchTab(1);

        //Verify title
        String title = "Test ISTQB Foundation en ligne";
        Assert.assertEquals(title, FondationTestPage.getTestPageTitle().getText());

        //Verify number of questions
        Assert.assertEquals(20, FondationTestPage.getQuestions().size());
    }

    @When("I fill answers correctly")
    public void iFillAnswersCorrectly() {
        List<WebElement> questions = FondationTestPage.getQuestions();
        for(int i=0; i<20; i++){
            WebElement question = questions.get(i);
            String response = Data.correctResponse[i];
            String xpath = ".//input[@value='"+ response +"']";
            DriverActions.makeVisible(xpath);
            //Click to fill answer. In case click is intercepted by chrono, scroll and retry
            try{
                question.findElement(By.xpath(xpath)).click();
            }catch (ElementClickInterceptedException e){
                DriverActions.scrollDown();
                question.findElement(By.xpath(xpath)).click();
            }

        }
    }

    @And("I send the form")
    public void iSendTheForm() {
        FondationTestPage.getSendFormButton().click();
    }

    @Then("I am asked to give an email")
    public void iAmAskedToGiveAnEmail() {
        String expectedTitle = "Résultats du test en ligne - Hightest";
        Assert.assertEquals(expectedTitle, EmailPage.getTitle().getText());
    }

    @When("I send my email")
    public void iSendMyEmail() {
        EmailPage.getEmailField().sendKeys(Data.email);
        EmailPage.getOkButton().click();
    }
}
