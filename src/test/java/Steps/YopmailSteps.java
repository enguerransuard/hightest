package Steps;

import Pages.HightestHomePage;
import Pages.ToolboxPage;
import Pages.YopmailPage;
import Storage.Data;
import Storage.DriverManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.testng.Assert;

public class YopmailSteps {

    @And("I open the mailbox")
    public void iOpenTheMailbox() {
        YopmailPage.getEmailField().sendKeys(Data.email);
        YopmailPage.getViewMailsButton().click();
    }

    @And("I open first mail")
    public void iOpenFirstMail() {
        DriverManager.driver.switchTo().frame("ifinbox");
        YopmailPage.getEmailsList().get(0).click();
        DriverManager.driver.switchTo().defaultContent();
    }

    @Then("the result of {string} % is displayed")
    public void theResultOfIsDisplayed(String result) {
        DriverManager.driver.switchTo().frame("ifmail");
        String sentence = YopmailPage.getEmailContent().findElement(By.xpath("./div/div/div[2]/div[1]")).getText();
        Assert.assertTrue(sentence.contains(result +" %"));
        DriverManager.driver.switchTo().defaultContent();
    }
}
