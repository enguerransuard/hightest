package Pages;

import Storage.DriverManager;
import Utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HightestHomePage {
    public static WebElement getHomepageMenu(){
        String xpath = "//*[@id='menu-sur-menu']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static WebElement getMenuByTitle(String title){
        String xpath = ".//*[@title='" + title + "']";
        return getHomepageMenu().findElement(By.xpath(xpath));
    }

    public static WebElement getAcceptCookiesButton(){
        String xpath = "//*[@id='cookie_action_close_header']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }
}
