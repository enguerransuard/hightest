package Pages;

import Storage.DriverManager;
import Utils.DriverActions;
import Utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ToolboxPage {
    public static WebElement getISTQBFondationByLanguage(String language){
        //get elem containing "test-istqb" in the link but not "agile". Language as a parameter
        String xpath = "//*[contains(@href, 'test-istqb') and not(contains(@href, 'agile')) and text()='" + language + "']";
        DriverActions.makeVisible(xpath);
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }
}
