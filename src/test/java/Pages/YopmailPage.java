package Pages;

import Storage.DriverManager;
import Utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class YopmailPage {
    public static WebElement getAcceptCookiesButton(){
        String xpath = "//*[@id='accept']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static WebElement getEmailField(){
        String xpath = "//*[@id='login']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static WebElement getViewMailsButton(){
        String xpath = "//*[@id='refreshbut']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static List<WebElement> getEmailsList(){
        String xpath = "//*[@class='lm']";
        return DriverManager.driver.findElements(By.xpath(xpath));
    }

    public static WebElement getEmailContent(){
        String xpath = "//*[@id='mail']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }
}
