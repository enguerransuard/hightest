package Pages;

import Storage.DriverManager;
import Utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EmailPage {
    public static WebElement getTitle(){
        String xpath = "//*[@id='header']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static WebElement getEmailField(){
        String xpath = "//*[@id='email']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static WebElement getOkButton(){
        String xpath = "//*[@id='submitMail']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }
}
