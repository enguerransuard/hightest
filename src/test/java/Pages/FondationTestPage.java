package Pages;

import Storage.DriverManager;
import Utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class FondationTestPage {

    public static WebElement getTestPageTitle(){
        String xpath = "//*[@id='header']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }

    public static List<WebElement> getQuestions(){
        //get all first answers and get parent node
        String xpath = "//input[@type='radio' and @value='1']/..";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElements(By.xpath(xpath));
    }

    public static WebElement getSendFormButton(){
        String xpath = "//*[@id='submit']";
        Waits.waitForElementVisible(xpath, 15);
        return DriverManager.driver.findElement(By.xpath(xpath));
    }
}
