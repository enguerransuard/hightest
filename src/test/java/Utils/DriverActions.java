package Utils;

import Storage.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Set;

public class DriverActions {

    public static void makeVisible(String xpath){
        WebElement element = DriverManager.driver.findElement(By.xpath( xpath));
        ((JavascriptExecutor)DriverManager.driver).executeScript("arguments[0].scrollIntoView({behavior: \"auto\", block: \"center\", inline: \"center\"})", element);
    }

    public static void scrollDown(){
        ((JavascriptExecutor)DriverManager.driver).executeScript("window.scrollBy(0, 500)");
    }
    public static void switchTab(int index){
        Set<String> handles = DriverManager.driver.getWindowHandles();
        DriverManager.driver.switchTo().window(handles.toArray()[index].toString());
    }
}
