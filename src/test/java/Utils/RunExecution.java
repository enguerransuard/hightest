package Utils;

import com.google.common.io.Files;
import io.cucumber.testng.FeatureWrapper;
import io.cucumber.testng.PickleWrapper;
import io.cucumber.testng.TestNGCucumberRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class RunExecution {

    private TestNGCucumberRunner testNGCucumberRunner;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(description = "Runs Feature", dataProvider = "scenarios")
    public void feature(PickleWrapper eventwrapper, FeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runScenario(eventwrapper.getPickle());
    }

    @DataProvider
    public Object[][] scenarios(){
        return testNGCucumberRunner.provideScenarios();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass(){
        if (testNGCucumberRunner == null) {
            return;
        }
        testNGCucumberRunner.finish();
    }

    //Method to create report
    public static void report(String reportName) throws IOException {
        String xmlFiles = System.getProperty("suiteXmlFile");
        File reportJson = new File("target/json/"+xmlFiles);
        if (!reportJson.exists()) {
            reportJson.mkdirs();
            reportJson.setWritable(true, false);
            reportJson.setReadable(true, false);
            reportJson.setExecutable(true, false);
        }
        Files.copy(new File("target/json/" + reportName + ".json"), new File("target/json/"+xmlFiles+"/" + reportName + ".json"));
    }
}
