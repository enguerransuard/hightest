package Utils;

import Storage.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waits {
    public static void waitForElementVisible(String xpath, int timeout){
        WebDriverWait wait = new WebDriverWait(DriverManager.driver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }

    public static void waitForElementClickable(String xpath, int timeout){
        WebDriverWait wait = new WebDriverWait(DriverManager.driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
    }
}
