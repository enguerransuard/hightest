package Runners;

import Storage.DriverManager;
import Utils.RunExecution;
import com.google.common.io.Files;
import io.cucumber.testng.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;

@CucumberOptions(
        features = {"src/test/ressources/features"},
        plugin = { "html:target/report/testngChrome.html", "json:target/json/reportChrome.json"},
        glue = {"Steps"},
        tags = "@ISTQB"
)
public class ChromeTest extends RunExecution {

    //Methods before each to test to set driver
    @BeforeMethod
    public void openDriver(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        DriverManager.driver = new ChromeDriver(options);
    }
    @AfterMethod
    public void tearDown(){
        DriverManager.driver.manage().deleteAllCookies();
        DriverManager.driver.quit();
    }

    @AfterTest
    public void createReport() throws IOException {
        RunExecution.report("reportChrome");
    }
}

