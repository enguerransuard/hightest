#Author: Enguerran Suard
@ISTQB
Feature: ISTQB test

  @Fondation @Français
  Scenario: fill a 100% ISTQB test
    When I open "https://hightest.nc" website
    And I acces french ISTQB fondation test
    Then the test is correctly displayed
    When I fill answers correctly
    And I send the form
    Then I am asked to give an email
    When I send my email
    And I open "https://yopmail.com/" website
    And I open the mailbox
    And I open first mail
    Then the result of "100" % is displayed
